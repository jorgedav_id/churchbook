package Utils;

public class Iglesia {
    private String imagenIglesia;

    public Iglesia() {

    }

    public Iglesia(String imagenIglesia) {
        this.imagenIglesia = imagenIglesia;
    }

    public String getImagenIglesia() {
        return imagenIglesia;
    }

    public void setImagenIglesia(String imagenIglesia) {
        this.imagenIglesia = imagenIglesia;
    }
}
