package Utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.churchbook.churchbook.R;

import java.util.List;

import App.AppController;

public class GaleriaIglesiasAdapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Iglesia> iglesiaItems;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public GaleriaIglesiasAdapter(Activity activity, List<Iglesia> iglesiaItems) {
        this.activity = activity;
        this.iglesiaItems = iglesiaItems;
    }

    @Override
    public int getCount() {
        return iglesiaItems.size();
    }

    @Override
    public Object getItem(int location) {
        return iglesiaItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.base_galeria, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();

        NetworkImageView imagen = (NetworkImageView) convertView.findViewById(R.id.ivSingleImagenIglesia);

        Iglesia iglesia = iglesiaItems.get(position);
        imagen.setImageUrl(iglesia.getImagenIglesia(), imageLoader);

        return convertView;
    }

}
