package com.churchbook.churchbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import App.AppController;
import Utils.GaleriaIglesiasAdapter;
import Utils.Iglesia;
import VolleyLibrary.Constantes;
import VolleyLibrary.CustomRequest;

public class Galeria extends AppCompatActivity {

    Toolbar actionBar;
    ListView galeriaIglesia;
    List<Iglesia> iglesiaItems;
    String nombreIglesia;
    GaleriaIglesiasAdapter listaAdapter;
    private ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galeria);
        setSupportActionBar(actionBar);
        inicializarComponentes();
        Intent intent = getIntent();
        nombreIglesia = intent.getStringExtra("direccionImagen");
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Cargando...");
        pDialog.show();
        makeJSONrequest();
    }

    private void inicializarComponentes() {
        actionBar = (Toolbar) findViewById(R.id.actionBar);
        galeriaIglesia = (ListView) findViewById(R.id.lvIglesias);
        iglesiaItems = new ArrayList<Iglesia>();
        listaAdapter = new GaleriaIglesiasAdapter(this, iglesiaItems);
        galeriaIglesia.setAdapter(listaAdapter);
    }

    private void makeJSONrequest() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("nombre", nombreIglesia);

        showProgressDialog();
        CustomRequest jsonObjReq = new CustomRequest(Request.Method.GET, Constantes.URL_GALERA, params,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        parseJson(response);
                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("OnErrorResponse", "Error: " + error.getMessage());
                hideProgressDialog();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }

    private void parseJson(JSONObject response) {
        try {
            JSONArray imagenesArray = response.getJSONArray("Iglesias");
            for (int i = 0; i < imagenesArray.length(); i++) {
                JSONObject imagenesIglesiaObj = (JSONObject) imagenesArray.get(i);
                Iglesia item = new Iglesia();
                String imageURL = imagenesIglesiaObj.isNull("Imagen") ? null : imagenesIglesiaObj
                        .getString("Imagen");
                item.setImagenIglesia(imageURL);
                iglesiaItems.add(item);
            }
            listaAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            Log.d("JSONException", e.toString());
        }
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }


}
