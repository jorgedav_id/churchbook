package com.churchbook.churchbook;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import Utils.CamaraPreview;

public class MainActivity extends AppCompatActivity {


    private static final int GALLERY_REQUEST = 2200;

    Toolbar actionBar;
    GalleryPhoto galleryPhoto;
    String selectedPhoto;
    String photoPath;
    String rutaImagen;
    FrameLayout contenedorCamara;
    Button botonTomarFoto;
    private Camera mCamara;
    private CamaraPreview mPreview;
    private static final int MEDIA_TYPE_IMAGE = 1;

    public String getRutaImagen() {
        return rutaImagen;
    }

    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inicializarComponentes();
        setSupportActionBar(actionBar);

        if (verificarCamara(this)) {
            mCamara = getCameraInstance();
            mCamara.setDisplayOrientation(90);
            mPreview = new CamaraPreview(this, mCamara);
            contenedorCamara.addView(mPreview);
        } else {
            Toast.makeText(this, "ERROR AL ACCEDER A CAMARA", Toast.LENGTH_SHORT).show();
        }

        botonTomarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCamara.takePicture(null, null, mPicture);
                String ruta = getRutaImagen();
                Intent intentClasificar = new Intent(MainActivity.this, InformacionIglesia.class);
                intentClasificar.putExtra("direccionImagen", ruta);
                startActivity(intentClasificar);
            }
        });
    }

    private void inicializarComponentes() {
        actionBar = (Toolbar) findViewById(R.id.actionBar);
        contenedorCamara = (FrameLayout) findViewById(R.id.ContenedorCamaraPreview);
        botonTomarFoto = (Button) findViewById(R.id.botonOk);
        galleryPhoto = new GalleryPhoto(getApplicationContext());
    }

    public boolean verificarCamara(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }

    public static Camera getCameraInstance() {
        Camera camara = null;
        try {
            camara = Camera.open();
        } catch (Exception ex) {
            Log.d("getCameraInstance: ", ex.getMessage());
        }
        return camara;
    }

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File archivoImagen = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            setRutaImagen(archivoImagen.getPath());
            if (archivoImagen == null) {
                Log.d("PICTURE CALLBACK ", "Error creando archivo");
            }
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(archivoImagen);
                fileOutputStream.write(data);
                fileOutputStream.close();
            } catch (FileNotFoundException fnf) {
                Log.d("PICTURE CALLBACK ", "archivo no encontrado");
            } catch (IOException io) {
                Log.d("PICTURE CALLBACK ", "Error de acceso a archivo");
            }
        }
    };

    private static File getOutputMediaFile(int type) {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "ChurchBook");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("CHURCHBOOK", "Error al crear carpeta");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("ddMMyyyy").format(new Date());
        File archivo;
        if (type == MEDIA_TYPE_IMAGE) {
            archivo = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp
                    + ".jpg");
        } else {
            return null;
        }
        return archivo;
    }

    private void liberarCamara() {
        if (mCamara != null) {
            mCamara.release();
            mCamara = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        liberarCamara();
    }

    @Override
    protected void onStop() {
        super.onStop();
        liberarCamara();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        liberarCamara();
    }


    private void escogerImagenGaleria() {
        Intent abrirGaleria;
        abrirGaleria = galleryPhoto.openGalleryIntent();
        startActivityForResult(abrirGaleria, GALLERY_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                galleryIntentResult(data);
            }
        }
    }

    private void galleryIntentResult(Intent data) {
        Uri uri = data.getData();
        galleryPhoto.setPhotoUri(uri);
        photoPath = galleryPhoto.getPath();
        selectedPhoto = photoPath;
        cargarFotoenImageV(photoPath);
    }

    private void cargarFotoenImageV(String photoPath) {
        try {
            Bitmap bitmap = ImageLoader.init().from(photoPath).requestSize(512, 512).getBitmap();
        } catch (FileNotFoundException error) {
            Toast.makeText(getApplicationContext(),
                    "Error al cargar Fotos", Toast.LENGTH_SHORT).show();
        }
    }

}
