package com.churchbook.churchbook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.kosalgeek.android.photoutil.ImageBase64;
import com.kosalgeek.android.photoutil.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import App.AppController;
import VolleyLibrary.Constantes;
import VolleyLibrary.CustomRequest;

public class InformacionIglesia extends AppCompatActivity {

    private String rutaImagen;
    private ImageView imagenIglesia;
    private Toolbar toolbarTitulo;
    Toolbar actionBar;
    private TextView informacionIglesia;
    private String TAG = InformacionIglesia.class.getSimpleName();
    private ProgressDialog pDialog;


    public String getRutaImagen() {
        return rutaImagen;
    }

    public void setRutaImagen(String rutaImagen) {
        this.rutaImagen = rutaImagen;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informacion_iglesia);
        setSupportActionBar(actionBar);
        inicializarComponentes();
        Intent nombreImagenGuardada = getIntent();
        setRutaImagen(nombreImagenGuardada.getStringExtra("direccionImagen"));
        System.out.println(getRutaImagen());
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Consultando...");
        pDialog.show();
        clasificarImagen();

        imagenIglesia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String nombreIglesia = toolbarTitulo.getTitle().toString();
                Intent galeriaIntent = new Intent(InformacionIglesia.this, Galeria.class);
                galeriaIntent.putExtra("direccionImagen", nombreIglesia);
                startActivity(galeriaIntent);
            }
        });
    }

    private void inicializarComponentes() {
        actionBar = (Toolbar) findViewById(R.id.actionBar);
        toolbarTitulo = (Toolbar) findViewById(R.id.toolbarNombreIglesia);
        imagenIglesia = (ImageView) findViewById(R.id.imagenIglesia);
        informacionIglesia = (TextView) findViewById(R.id.informacionIglesia);
    }

    public void clasificarImagen() {
        String encodeImage = convertirImagen(getRutaImagen());
        Map<String, String> params = new HashMap<>();
        params.put("file", encodeImage);

        showProgressDialog();
        CustomRequest jsonObjReq = new CustomRequest(Request.Method.POST, Constantes.URL_CLASIFICAR, params,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("david", response.toString());
                        parseJson(response);
                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideProgressDialog();
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq);
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(500 * 1000, 0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    private String convertirImagen(String rutaImagen) {
        Bitmap bitmap;
        String encodedImage = null;
        try {
            bitmap = ImageLoader.init().from(rutaImagen).requestSize(1024, 1024).getBitmap();
            encodedImage = ImageBase64.encode(bitmap);
        } catch (FileNotFoundException e) {
            Toast.makeText(getApplicationContext(),
                    "Error al codificar Foto", Toast.LENGTH_SHORT).show();
        }
        return encodedImage;

    }

    private void parseJson(JSONObject response) {
        try {
            String titulo = response.getString("cTitulo");
            String descIglesia = response.getString("cDescripcion");
            informacionIglesia.setText(descIglesia);
            imagenIglesia.setImageURI(Uri.parse(getRutaImagen()));
            toolbarTitulo.setTitle(titulo);

        } catch (JSONException e) {
            Log.d("JSONException", e.toString());
        }
    }
}
